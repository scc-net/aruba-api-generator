from .helpers import translate_json_data_type
from operator import itemgetter


def spec2functions(spec: dict) -> dict:
    functions = {}
    for path, methods in spec['paths'].items():
        func_methods = []
        if methods:
            for method, definition in methods.items():
                func_method = {'path': path, 'method': method, 'parameters': [],
                               'returns_data': True if 'schema' in definition['responses']['200'] else False}
                for parameter in definition['parameters']:
                    if parameter['name'] != 'X-CSRF-Token':
                        if 'schema' in parameter:
                            properties = parse_object_properties(parameter['schema'])
                            for prop in properties:
                                tmp = prop
                                tmp['name'] = f'{parameter["name"]}_{prop["name"]}'
                                tmp['in'] = parameter['in']
                                func_method['parameters'].append(tmp)
                        elif 'type' in parameter:
                            tmp = {'name': parameter['name'], 'required': parameter['required'], 'in': parameter['in'],
                                   'type': translate_json_data_type(parameter['type'])}
                            func_method['parameters'].append(tmp)
                        else:
                            raise ValueError(f'No type found for parameter "{parameter["name"]}" in function "{path}.{method}"')
                signature = [f'{param["name"]}: {param["type"]}{" = None" if not param["required"] else ""}' for param in func_method['parameters']]
                func_method['signature'] = ", ".join(signature)
                func_method['body'] = ", ".join([f'"{k["name"]}": {k["name"]}' for k in func_method['parameters'] if k["in"] == "body"])
                func_method['query'] = "&".join([f'{k["name"]}={{{k["name"]}}}' for k in func_method['parameters'] if k["in"] == "query"])
                func_methods.append(func_method)
            functions[str(path.split('/')[-1])] = func_methods
    return functions


def spec2objects(spec: dict) -> dict:
    objects = {}
    functions = spec2functions(spec)
    for name, obj in spec['definitions'].items():
        definition = {'name': name, 'properties': parse_object_properties(obj), 'functions': functions[name]}
        objects[name] = definition
    return objects


def parse_object_properties(obj: dict) -> list:
    if 'properties' in obj:
        schema = sorted([{
            'name': prop.replace('-', '_'),
            'type': translate_json_data_type(attrs['type']),
            'required': True if 'required' in obj and prop in obj['required'] else False,
        } for prop, attrs in obj['properties'].items()], key=itemgetter('required'), reverse=True)
        return schema
    else:
        return []
