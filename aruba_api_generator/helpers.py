
def translate_json_data_type(t: str) -> str:
    if t == 'boolean':
        return 'bool'
    if t == 'number' or t == 'integer':
        return 'int'
    if t == 'string' or t == 'json':
        return 'str'
    if t == 'array':
        return 'list'
    if t == 'object':
        return 'dict'
    raise ValueError(t)
